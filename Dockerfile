FROM golang:1.20.4 as builder
RUN adduser --system appuser

WORKDIR /cse

ENV GO111MODULE=on
ENV GOPRIVATE=gitlab.switch.ch/ub-unibas/*
ENV CGO_ENABLED=0
ENV GOOS=linux
ENV GOARCH=amd64

COPY . .
WORKDIR cmd/cse
RUN go build -o bin/app

FROM scratch
WORKDIR /app
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /cse/cmd/cse/bin/app /app/app
COPY --from=builder /etc/passwd /etc/passwd

USER appuser

ADD web/static /app/static/
ADD web/template /app/template/

ENTRYPOINT ["/app/app"]
